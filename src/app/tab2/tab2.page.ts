import { Component } from '@angular/core';
import { DataLocalService } from '../services/data-local.service';
import { Registro } from '../models/registro.model'
import { OnInit, ChangeDetectionStrategy } from '@angular/core';


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class Tab2Page {

  registros: Registro[] = [];

  constructor(public dataLocal: DataLocalService) {
    this.refresh();
  }

  abrirRegistro(registro: Registro) {
    console.log(registro.type + "----------------------");
    this.dataLocal.abrirRegistro(registro);
  }

  ionViewDidEnter(){
    this.refresh();
  }

  refresh() {
    this.dataLocal.cargarStorage();
    this.registros = this.dataLocal.guardados; 
  }

  enviarCorreo() {
    this.dataLocal.enviarCorreo();
  }


}
