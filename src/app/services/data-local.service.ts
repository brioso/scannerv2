import { Injectable } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { NavController } from '@ionic/angular';
import { Registro } from '../models/registro.model';
import {Storage} from '@ionic/storage-angular'

import { File } from '@ionic-native/file/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';


@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

guardados: Registro[] = [];

  constructor(private storage: Storage, private navController: NavController, private inAppBrowser: InAppBrowser, private file: File, private emailComposer: EmailComposer) {
    this.storage.create();
    this.cargarStorage();
  }

  async cargarStorage() {
    this.guardados = await this.storage.get('registros') || [];
  }

  guardarRegistro(format: string, text: string) {
    this.cargarStorage();

    console.log(format);

    const nuevoRegistro = new Registro(format, text);
    this.guardados.push(nuevoRegistro);
    this.storage.set('registros', this.guardados);

    this.abrirRegistro(nuevoRegistro);
  }

  abrirRegistro(registro: Registro) {
    console.log("fjkdnkdngkngkgnfkjjjjjjjjjjjjjjjj" + registro.type);
    this.navController.navigateForward('tabs/tab2');
    switch(registro.type){
      case 'http':
        const browser=this.inAppBrowser.create(registro.text,'_system');
        break;
      case 'geo':
        this.navController.navigateForward(`/tabs/tab2/mapa/${registro.text}`);
        break;
 

    }
  }


  enviarCorreo() {
    const temp = [];
    const titulos: string = "Tipo, Formato, Creado en, Tenxton\n";

    temp.push(titulos);
    this.guardados.forEach(element => {
      const linea = `${element.type}, ${element.format}, ${element.created}, ${element.text.replace(',', ' ')}\n`;
      temp.push(linea);
    });

    this.crearArchivoFisico(temp.join(''));
  }

  crearArchivoFisico(text: string) {
    this.file.checkFile(this.file.dataDirectory, 'registros.csv').then(
      exists => {
        return this.escribirEnArchivo(text);
      }
    ).catch (err => {
      return this.file.createFile(this.file.dataDirectory, 'registros.csv', false)
      .then(creado => this.escribirEnArchivo(text))
      .then(err2 => console.log('No se pudo crear el archivo'));
    });
  }

  async escribirEnArchivo(text: string) {
    await this.file.writeExistingFile(this.file.dataDirectory, 'registros.csv', text);

    const archivo = `${this.file.dataDirectory}registros.csv`;
    let email = {
      to: 'paula.brioso.7e3@itb.cat',
      //cc
      //bcc
      attachments: [
        archivo
      ],
      subject: 'Backup de scansss',
      body: 'Copia de todos los scant <string>bby</strong>',
      isHtml: true
    }

    this.emailComposer.open(email);
  }

}
